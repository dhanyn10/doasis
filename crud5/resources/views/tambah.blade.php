@extends('template')
@section('konten')
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header">Tambah Data Pegawai</div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Foto</label>
                    </div>
                    <div class="form-group">
                        <input type="file" name="photo" id="photo" required>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>gender</label>
                    </div>
                    <div class="form-group">
                        <select name="gender" class="form-control" required>
                            <option value="L">Laki-laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                    </div>
                    <div class="form-group">
                        <input type="number" name="nip" id="" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label>Hobi</label>
                    </div>
                    <div class="form-group">
                        <select name="hoby" class="form-control" required>
                            <option value="sepak bola">Sepak Bola</option>
                            <option value="voli">Sepak Bola</option>
                            <option value="tenis meja">Tenis Meja</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
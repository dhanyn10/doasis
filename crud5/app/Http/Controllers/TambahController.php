<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class TambahController extends Controller
{
    public function formtambah(Request $req)
    {
        $validasi = Validator::make($req->all(), [
            'name'      => 'required',
            'email'     => 'required',
            'gender'    => 'required|in:L,P',
            'nip'       => 'required|digits',
            'hoby'      => 'required|in:sepak bola, voli, tenis meja',
            'photo'     => 'required|mimes:png,jpg'
        ]);
        if($validasi->fails())
        {
            flash('error')->error();
        }
        else
        {
            //
        }
    }
}

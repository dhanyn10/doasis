<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form method="post" action="index.php">
            <input type="text" name="anagram">
            <button type="submit">Go</button>
        </form>
        <?php
            if(isset($_POST['anagram']))
            {
                $str = explode(" ", $_POST['anagram']);
                $str0 = $str[0];
                $str1 = $str[1];

                if(count_chars($str0, 1) == count_chars($str1, 1))
                    echo "true";
                else
                    echo "false";
            }
        ?>
    </body>
</html>